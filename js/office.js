/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gOFFICE_URL = "http://localhost:8080/api/offices";
const gCOLUMN_ID = {
    stt: 0,
    action:1,
    city: 2,
    phone: 3,
    addressLine: 4,
    state: 5,
    country: 6,
    territory: 7,

}
const gCOL_NAME = [
    "id",
    "action",
    "city",
    "phone",
    "addressLine",
    "state",
    "country",
    "territory"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getOfficeList() {
        this.vApi.onGetOfficesClick((paramoffice) => {
            this._createOfficeTable(paramoffice)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createOfficeTable(paramoffice) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-office')) {
            $('#table-office').DataTable().destroy();
        }
        const vOrderTable = $("#table-office").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.city] },
                { "data": gCOL_NAME[gCOLUMN_ID.phone] },
                { "data": gCOL_NAME[gCOLUMN_ID.addressLine] },
                { "data": gCOL_NAME[gCOLUMN_ID.state] },
                { "data": gCOL_NAME[gCOLUMN_ID.country] },
                { "data": gCOL_NAME[gCOLUMN_ID.territory] },
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets:gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-office" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-office" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-office').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-office').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramoffice) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "office.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getOfficeList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-office").on("click", () => {
            $("#create-office-modal").modal("show")
            this._renderPhotoList()
        })
    }


    _createOffice() {
        this._clearInput();
      
        $("#btn-create-office").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
          const vFields = [
            "input-create-city",
            "input-create-phone",
            "input-create-addressLine",
            "input-create-state",
            "input-create-country",
            "input-create-territory",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const officeData = {
              city: $("#input-create-city").val().trim(),
              phone: $("#input-create-phone").val().trim(),
              addressLine: $("#input-create-addressLine").val().trim(),
              state: $("#input-create-state").val().trim(),
              country: $("#input-create-country").val().trim(),
              territory: $("#input-create-territory").val().trim(),
            };
            
            this.vApi.onCreateOfficeClick(officeData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      _updateOffice(office) {
        $('#input-update-city').val(office.city);
        $('#input-update-phone').val(office.phone);
        $('#input-update-addressLine').val(office.addressLine);
        $('#input-update-state').val(office.state);
        $('#input-update-country').val(office.country);
        $('#input-update-territory').val(office.territory);
      
        $('#btn-update-office').off('click').on('click', () => {
          this._clearInValid();
          let isValid = true;
          const requiredFields = ['input-update-city', 'input-update-phone', 'input-update-addressLine', 'input-update-state', 'input-update-country', 'input-update-territory'];
      
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass('is-invalid');
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
      
          if (isValid) {
            const updatedOffice = {
              city: $('#input-update-city').val().trim(),
              phone: $('#input-update-phone').val().trim(),
              addressLine: $('#input-update-addressLine').val().trim(),
              state: $('#input-update-state').val().trim(),
              country: $('#input-update-country').val().trim(),
              territory: $('#input-update-territory').val().trim(),
            };
      
            this.vApi.onUpdateOfficeClick(office.id, updatedOffice, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      

    _deleteoffice(data) {
        $("#btn-confirm-delete-office").on("click", () => {
            this.vApi.onDeleteOfficeClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-office-form").find("input").val("");
        $("#create-office-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createOffice()
        } else {
            if (type === "edit") {
                this._updateOffice(data)
                $("#update-office-modal").modal("show")
            } else {
                this._deleteoffice(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
    onGetPhotosClick(paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetPhotoByIdClick(photoId, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetOfficesClick(paramCallbackFn) {
        $.ajax({
            url: gOFFICE_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetOfficeByIdClick(officeId, paramCallbackFn) {
        $.ajax({
            url: gOFFICE_URL + "/" + officeId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateOfficeClick(officeData, paramCallbackFn) {
        $.ajax({
            url: gOFFICE_URL,
            method: 'POST',
            data: JSON.stringify(officeData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo office thành công!!")
                $("#create-office-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateOfficeClick(officeId, officeData, paramCallbackFn) {
        $.ajax({
            url: gOFFICE_URL + "/" + officeId,
            method: 'PUT',
            data: JSON.stringify(officeData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-office-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa office thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteOfficeClick(officeId) {
        $.ajax({
            url: gOFFICE_URL + "/" + officeId,
            method: 'DELETE',
            success:  ()=> {
                this.onShowToast("Xóa thành công", "bạn đã xóa office thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/offices/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

}