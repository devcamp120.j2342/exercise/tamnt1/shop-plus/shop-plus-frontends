/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPRODUCT_URL = "http://localhost:8080/api/products";
const gPRODUCT_LINE_URL = "http://localhost:8080/api/product-lines";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    productCode: 2,
    productName: 3,
    productDescription: 4,
    productScale: 5,
    productVendor: 6,
    quantityInStock: 7,
    buyPrice: 8,

}
const gCOL_NAME = [
    "action",
    "id",
    "productCode",
    "productName",
    "productDescription",
    "productScale",
    "productVendor",
    "quantityInStock",
    "buyPrice"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getproductList() {
        this._createProductTable()
    }

    //Hàm tạo các thành phần của bảng
    _createProductTable() {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-product')) {
            $('#table-product').DataTable().destroy();
        }
        const vOrderTable = $("#table-product").DataTable({
            // Khai báo các cột của datatable
            processing: true, // shows loading image while fetching data
            serverSide: true, // activates server side pagination
            ajax: {
                // headers: {
                //     Authorization: "Bearer " + this.token,
                // },
                url: gPRODUCT_URL + "/dataTable", // API
            },
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            columns: [{ data: gCOL_NAME[0] }, { data: gCOL_NAME[1] }, { data: gCOL_NAME[2] }, { data: gCOL_NAME[3] }, {
                data: gCOL_NAME[4], render: function (data, type, row) {
                    if (type === "display" && data.length > 50) {
                        return data.substr(0, 50) + "...";
                    }
                    return data;
                }
            }, { data: gCOL_NAME[5] }, { data: gCOL_NAME[6] }, { data: gCOL_NAME[7] }, { data: gCOL_NAME[8] }],
            order: [[1, "asc"]],
            scrollX: true,

            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: 1,
                    render: function () {
                        return stt++
                    }
                },
                {
                    targets: gCOLUMN_ID.photos,
                    render: function (data) {
                        let photoNames = '';
                        if (Array.isArray(data)) {
                            data.forEach((photo, index) => {
                                photoNames += photo.photoName;
                                if (index < data.length - 1) {
                                    photoNames += ', ';
                                }
                            });
                        }
                        return photoNames;
                    }
                },
                {

                    targets: 0,
                    defaultContent: `
                              <img class="edit-product" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-product" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === 0) {
                            $(cell).find('.edit-product').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-product').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        $('.table-container').css('overflow-x', 'auto');
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "product.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getproductList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-product").on("click", () => {
            $("#create-product-modal").modal("show")
        })
    }

    _getProductLineList(type, data) {
        let prooductLineDropdown
        if (type === "update") {
            prooductLineDropdown = $("#input-update-productLine");
        } else {
            prooductLineDropdown = $("#input-create-productLine");
        }
        this.vApi.onGetProductsLineClick((productLines) => {
            prooductLineDropdown.empty();
            prooductLineDropdown.append(`<option value="" selected>${data?.productLine?.productLine || "Product Line"}</option>`);

            productLines.forEach((product) => {
                const option = `<option value="${product.id}">${product.productLine}</option>`;
                prooductLineDropdown.append(option);
            });
        });
    }
    async _imgUpload(type, imageArray) {
        let imageURL;
        let imageWrapper;
        let uploadInput;
        if (type === "create") {
            imageURL = [];
            imageWrapper = $(".upload__img-wrap");
            uploadInput = $(".upload__inputfile");
        } else {
            imageURL = [...imageArray];
            imageWrapper = $(".upload__img-wrap-update");
            uploadInput = $(".upload__inputfile-update");
        }
        uploadInput.off('change');
        // Bind the change event handler outside the loop
        uploadInput.on('change', async (e) => {
            var files = e.target.files;
    
            if (files.length > 0) {
                const formDataArray = [];
    
                // Create an array of promises for each file upload
                for (let i = 0; i < files.length; i++) {
                    const formData = new FormData();
                    formData.append("image", files[i]);
    
                    formDataArray.push(this.vApi.onUploadFile(formData)); // Assuming onUploadFile returns a promise
                }
    
                // Wait for all promises to resolve
                try {
                    $(".upload__img-wrap-update").empty()
                    const responses = await Promise.all(formDataArray);
                    imageURL.push(...responses);
    
                    // Check type and append images based on it
                    const htmlTemplate = type === "create" ? "upload__img-box" : "upload__img-box-update";
                    imageURL.forEach((image) => {
                        var html = `<div class='${htmlTemplate}'><div style='background-image: url(${image})' data-number='${$(".upload__img-close").length}' data-file='${image}' class='img-bg'><div class='upload__img-close'></div></div></div>`;
                        imageWrapper.append(html);
                    });
                } catch (error) {
                    console.error("Error uploading files:", error);
                }
            }
        });
    
        return imageURL;
    }
    

    async _createProduct() {
        this._clearInput();
        const imageURL = await this._imgUpload("create");
        let newProductLine

        $("#input-create-productLine").on("change", (event) => {
            const id = event.target.value
            this.vApi.onGetProductLineByIdClick(id, (paramData) => {
                newProductLine = paramData
            })
        })
        $('body').on('click', ".upload__img-close", function (e) {
            var file = $(this).parent().data("file");
            for (var i = 0; i < imageURL.length; i++) {
                if (imageURL[i] === file) {
                    imageURL.splice(i, 1);
                    console.log(imageURL)
                    break;
                }
            }
            $(this).parent().parent().remove();
        });
        this._getProductLineList("create")
        $("#btn-create-product").on("click", () => {
            this._clearInValid();

            let isValid = true;

            const vFields = [
                "input-create-name"
            ];

            vFields.forEach((field) => {
                const fieldValue = $(`#${field}`).val().trim();

                if (!fieldValue || fieldValue === "") {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const productData = {
                    productCode: $("#input-create-productCode").val().trim(),
                    productName: $("#input-create-name").val().trim(),
                    productDescription: $("#input-create-desc").val().trim(),
                    productScale: $("#input-create-scale").val().trim(),
                    productVendor: $("#input-create-vendor").val().trim(),
                    quantityInStock: parseInt($("#input-create-qty").val().trim()),
                    buyPrice: parseFloat($("#input-create-buyPrice").val().trim()),
                    productLine: newProductLine,
                    productPhotos: imageURL,
                    orderDetails: []
                };

                this.vApi.onCreateProductClick(productData, (responseData) => {
                    console.log(responseData);
                });

            }
        }).bind(this);
    }

    async _updateProduct(data) {
        this._clearInput();
        $(".upload__img-wrap-update").empty()
        var imageWrapper = $(".upload__img-wrap-update");
        this._getProductLineList("update", data)
        const imageURL = await this._imgUpload("update", data.productPhotos);
        $("#input-update-productCode").val(data.productCode);
        $("#input-update-name").val(data.productName);
        $("#input-update-desc").val(data.productDescription);
        $("#input-update-scale").val(data.productScale);
        $("#input-update-vendor").val(data.productVendor);
        $("#input-update-qty").val(data.quantityInStock);
        $("#input-update-buyPrice").val(data.buyPrice);
        let newProductLine = data.productLine

        $("#input-update-productLine").on("change", (event) => {
            const id = event.target.value
            this.vApi.onGetProductLineByIdClick(id, (paramData) => {
                newProductLine = paramData
            })
        })
        if (data.productPhotos.length > 0) {
            data.productPhotos.forEach((image) => {
                var html = "<div class='upload__img-box-update'><div style='background-image: url(" + image + ")' data-number='" + $(".upload__img-close").length + "' data-file='" + image + "' class='img-bg'><div class='upload__img-close'></div></div></div>";
                imageWrapper.append(html);
            });

        }

        $('body').on('click', ".upload__img-close", function (e) {
            var file = $(this).parent().data("file");
            for (var i = 0; i < imageURL.length; i++) {
                if (imageURL[i] === file) {
                    imageURL.splice(i, 1);
                    console.log(imageURL)
                    break;
                }
            }
            $(this).parent().parent().remove();
        });
        $("#btn-update-product").off("click").on("click", () => {
            this._clearInValid();
            let isValid = true;
            const requiredFields = [
                "input-update-name",
            ];

            requiredFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
                }
            });

            if (isValid) {
                const updatedProduct = {
                    productCode: $("#input-update-productCode").val().trim(),
                    productName: $("#input-update-name").val().trim(),
                    productDescription: $("#input-update-desc").val().trim(),
                    productScale: $("#input-update-scale").val().trim(),
                    productVendor: $("#input-update-vendor").val().trim(),
                    quantityInStock: parseInt($("#input-update-qty").val()),
                    buyPrice: parseFloat($("#input-update-buyPrice").val()),
                    productLine:newProductLine,
                    orderDetails: [],
                    productPhotos:imageURL
                };

                this.vApi.onUpdateProductClick(data.id, updatedProduct, (responseData) => {
                    console.log(responseData);
                });

            }
        }).bind(this);
    }



    _deleteProduct(data) {
        $("#btn-confirm-delete-product").on("click", () => {
            this.vApi.onDeleteProductClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-product-form").find("input").val("");
        $("#create-product-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createProduct()
        } else {
            if (type === "edit") {
                this._updateProduct(data)
                $("#update-product-modal").modal("show")
            } else {
                this._deleteProduct(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }


    onGetProductsClick(paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetProductsLineClick(paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_LINE_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetProductByIdClick(productId, paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_URL + "/" + productId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetProductLineByIdClick(productId, paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_LINE_URL + "/" + productId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateProductClick(productData, paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_URL,
            method: 'POST',
            data: JSON.stringify(productData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("created successfully", "you have created successfully!!")
                $("#create-product-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateProductClick(productId, productData, paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_URL + "/" + productId,
            method: 'PUT',
            data: JSON.stringify(productData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-product-modal').modal('hide');
                this.onShowToast("edited successfully", "you have edited successfully!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteProductClick(productId) {
        $.ajax({
            url: gPRODUCT_URL + "/" + productId,
            method: 'DELETE',
            success: () => {
                this.onShowToast("deleted successfully", "you have deleted successfully!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onUploadFile(formData) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: gUPLOAD_FILE_URL,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    resolve(response);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('Error:', errorThrown);
                    reject(errorThrown);
                }
            });
        });
    }



    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/products/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}