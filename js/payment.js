/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPAYMENT_URL = "http://localhost:8080/api/payment";

const gCOLUMN_ID = {
    stt: 0,
    action:1,
    checkNumber: 2,
    paymentDate: 3,
    ammount: 4,

}
const gCOL_NAME = [
    "stt",
    "action",
    "checkNumber",
    "paymentDate",
    "ammount",

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getPaymentList() {
        this.vApi.onGetPaymentsClick((parampayment) => {
            this._createPaymentTable(parampayment)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createPaymentTable(parampayment) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-payment')) {
            $('#table-payment').DataTable().destroy();
        }
        const vOrderTable = $("#table-payment").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.checkNumber] },
                { "data": gCOL_NAME[gCOLUMN_ID.paymentDate] },
                { "data": gCOL_NAME[gCOLUMN_ID.ammount] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-payment" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-payment" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-payment').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-payment').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(parampayment) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "payment.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getPaymentList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-payment").on("click", () => {
            $("#create-payment-modal").modal("show")
            this._renderPhotoList()
        })
    }

    _createPayment() {
        this._clearInput();
        $("#btn-create-payment").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
          const vFields = [
            "input-create-checkNumber",
            "input-create-paymentDate",
            "input-create-amount"
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const paymentData = {
              checkNumber: $("#input-create-checkNumber").val().trim(),
              paymentDate: $("#input-create-paymentDate").val().trim(),
              ammount: parseFloat($("#input-create-amount").val().trim())
            };
            this.vApi.onCreatePaymentClick(paymentData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      _updatePayment(payment) {
        $("#input-update-checkNumber").val(payment.checkNumber);
        $("#input-update-paymentDate").val(payment.paymentDate);
        $("#input-update-amount").val(payment.ammount);
      
        $("#btn-update-payment").off("click").on("click", () => {
          this._clearInValid();
          let isValid = true;
          const requiredFields = ["input-update-checkNumber", "input-update-paymentDate", "input-update-amount"];
      
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
      
          if (isValid) {
            const updatedPayment = {
              checkNumber: $("#input-update-checkNumber").val().trim(),
              paymentDate: $("#input-update-paymentDate").val().trim(),
              ammount: parseFloat($("#input-update-amount").val().trim())
            };
      
            this.vApi.onUpdatePaymentClick(payment.id, updatedPayment, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      

    _deletePayment(data) {
        $("#btn-confirm-delete-payment").on("click", () => {
            this.vApi.onDeletePaymentClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-payment-form").find("input").val("");
        $("#create-payment-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createPayment()
        } else {
            if (type === "edit") {
                this._updatePayment(data)
                $("#update-payment-modal").modal("show")
            } else {
                this._deletePayment(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }


    onGetPaymentsClick(paramCallbackFn) {
        $.ajax({
            url: gPAYMENT_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetPaymentByIdClick(paymentId, paramCallbackFn) {
        $.ajax({
            url: gPAYMENT_URL + "/" + paymentId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreatePaymentClick(paymentData, paramCallbackFn) {
        $.ajax({
            url: gPAYMENT_URL,
            method: 'POST',
            data: JSON.stringify(paymentData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo payment thành công!!")
                $("#create-payment-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdatePaymentClick(paymentId, paymentData, paramCallbackFn) {
        $.ajax({
            url: gPAYMENT_URL + "/" + paymentId,
            method: 'PUT',
            data: JSON.stringify(paymentData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-payment-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa payment thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeletePaymentClick(paymentId) {
        $.ajax({
            url: gPAYMENT_URL + "/" + paymentId,
            method: 'DELETE',
            success:  () =>{
                this.onShowToast("Xóa thành công", "bạn đã xóa payment thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/payments/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}