/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gEMPLOYEE_URL = "http://localhost:8080/api/employees";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    lastName: 2,
    firstName: 3,
    extension: 4,
    email: 5,
    officeCode: 6,
    reportTo: 7,
    jobTitle: 8,



}
const gCOL_NAME = [
    "action",
    "id",
    "lastName",
    "firstName",
    "extension",
    "email",
    "officeCode",
    "reportTo",
    "jobTitle"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getEmployeeList() {
        this.vApi.onGetEmployeesClick((paramEmployee) => {
            this._createEmployeeTable(paramEmployee)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createEmployeeTable(paramEmployee) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-employee')) {
            $('#table-employee').DataTable().destroy();
        }
        const vOrderTable = $("#table-employee").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            processing: true, // shows loading image while fetching data
            serverSide: true, // activates server side pagination
            ajax: {
                // headers: {
                //     Authorization: "Bearer " + this.token,
                // },
                url: gEMPLOYEE_URL + "/dataTable", // API
            },
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            columns: [{ data: gCOL_NAME[0] }, { data: gCOL_NAME[1] }, { data: gCOL_NAME[2] }, { data: gCOL_NAME[3] }, { data: gCOL_NAME[4] }, { data: gCOL_NAME[5] }, { data: gCOL_NAME[6] }, { data: gCOL_NAME[7] }, { data: gCOL_NAME[8] }],
            order: [[1, "asc"]],
            scrollX: true,
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: 0,
                    defaultContent: `
                              <img class="edit-employee" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-employee" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === 0) {
                            $(cell).find('.edit-employee').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-employee').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        $('.table-container').css('overflow-x', 'auto');
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "employee.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getEmployeeList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-employee").on("click", () => {
            $("#create-employee-modal").modal("show")

        })
    }

    _createEmployee() {
        this._clearInput();
        $("#btn-create-employee").on("click", () => {
            this._clearInValid();

            let isValid = true;
            const vFields = [
                "input-create-firstName",
                "input-create-lastName",
                "input-create-email",
                "input-create-report",
                "input-create-jobTitle",
                "input-create-code",
                "input-create-ext"
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const employeeData = {
                    lastName: $("#input-create-lastName").val().trim(),
                    firstName: $("#input-create-firstName").val().trim(),
                    extension: $("#input-create-ext").val().trim(),
                    email: $("#input-create-email").val().trim(),
                    officeCode: parseInt($("#input-create-code").val().trim()),
                    reportTo: parseInt($("#input-create-report").val().trim()),
                    jobTitle: $("#input-create-jobTitle").val().trim(),

                };

                console.log(employeeData)
                this.vApi.onCreateEmployeeClick(employeeData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }

    _updateEmployee(employee) {
        $('#input-update-firstName').val(employee.firstName);
        $('#input-update-lastName').val(employee.lastName);
        $('#input-update-email').val(employee.email);
        $('#input-update-report').val(employee.reportTo);
        $('#input-update-jobTitle').val(employee.jobTitle);
        $('#input-update-code').val(employee.officeCode);
        $('#input-update-ext').val(employee.extension);

        $('#btn-update-employee').off('click').on('click', () => {
            this._clearInValid();
            let isValid = true;
            const requiredFields = [
                'input-update-firstName',
                'input-update-lastName',
                'input-update-email',
                'input-update-report',
                'input-update-jobTitle',
                'input-update-code',
                'input-update-ext'
            ];

            requiredFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass('is-invalid');
                    $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
                }
            });

            if (isValid) {
                const updatedEmployee = {
                    lastName: $("#input-update-lastName").val().trim(),
                    firstName: $("#input-update-firstName").val().trim(),
                    extension: $("#input-update-ext").val().trim(),
                    email: $("#input-update-email").val().trim(),
                    officeCode: parseInt($("#input-update-code").val().trim()),
                    reportTo: parseInt($("#input-update-report").val().trim()),
                    jobTitle: $("#input-update-jobTitle").val().trim(),
                };

                this.vApi.onUpdateEmployeeClick(employee.id, updatedEmployee, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }


    _deleteEmployee(data) {
        $("#btn-confirm-delete-employee").on("click", () => {
            this.vApi.onDeleteEmployeeClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-employee-form").find("input").val("");
        $("#create-employee-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createEmployee()
        } else {
            if (type === "edit") {
                this._updateEmployee(data)
                $("#update-employee-modal").modal("show")
            } else {
                this._deleteEmployee(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
    onGetPhotosClick(paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }



    onGetEmployeesClick(paramCallbackFn) {
        $.ajax({
            url: gEMPLOYEE_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetEmployeeByIdClick(employeeId, paramCallbackFn) {
        $.ajax({
            url: gEMPLOYEE_URL + "/" + employeeId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateEmployeeClick(employeeData, paramCallbackFn) {
        $.ajax({
            url: gEMPLOYEE_URL,
            method: 'POST',
            data: JSON.stringify(employeeData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo employee thành công!!")
                $("#create-employee-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateEmployeeClick(employeeId, employeeData, paramCallbackFn) {
        $.ajax({
            url: gEMPLOYEE_URL + "/" + employeeId,
            method: 'PUT',
            data: JSON.stringify(employeeData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-employee-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa employee thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteEmployeeClick(employeeId) {
        $.ajax({
            url: gEMPLOYEE_URL + "/" + employeeId,
            method: 'DELETE',
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa employee thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/employees/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }



}