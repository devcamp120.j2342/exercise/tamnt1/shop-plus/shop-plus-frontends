/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gORDERDETAIL_URL = "http://localhost:8080/api/order-details";

const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    quantityOrder: 2,
    priceEach: 3,

}
const gCOL_NAME = [
    "action",
    "id",
    "product",
    "quantityOrder",
    "priceEach",

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    _getOrderDetailList() {
        this._createOrderDetailTable()

    }

    //Hàm tạo các thành phần của bảng
    _createOrderDetailTable() {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-orderDetail')) {
            $('#table-orderDetail').DataTable().destroy();
        }
        const vOrderTable = $("#table-orderDetail").DataTable({
            // Khai báo các cột của datatable
            processing: true, // shows loading image while fetching data
            serverSide: true, // activates server side pagination
            ajax: {
                // headers: {
                //     Authorization: "Bearer " + this.token,
                // },
                url: gORDERDETAIL_URL + "/dataTable", // API
            },
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            columns: [{ data: gCOL_NAME[0] }, { data: gCOL_NAME[1] }, { data: gCOL_NAME[2],   render: function (data, type, row) {
                // Modify this column to show the name from the "customer" object
                if (row.product) {
                    return row.product.productCode;
                }
                return '';
            } }, { data: gCOL_NAME[3] }, { data: gCOL_NAME[4] }],
            order: [[1, "asc"]],
            scrollX: true,

            // Ghi đè nội dung của cột action
            "columnDefs": [

                {
                    targets: 1,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: 0,
                    defaultContent: `
                              <img class="edit-orderDetail" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-orderDetail" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === 0) {
                            $(cell).find('.edit-orderDetail').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-orderDetail').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
    
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "order-detail.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getOrderDetailList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-orderDetail").on("click", () => {
            $("#create-orderDetail-modal").modal("show")
        })
    }

    _createOrderDetail() {
        this._clearInput();
        $("#btn-create-orderDetail").on("click", () => {
            this._clearInValid();

            let isValid = true;
            const requiredFields = ["input-create-quantity", "input-create-price"];

            requiredFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const orderDetailData = {
                    quantityOrder: $("#input-create-quantity").val().trim(),
                    priceEach: parseFloat($("#input-create-price").val().trim()),
                };
                this.vApi.onCreateOrderDetailClick(orderDetailData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }

    _updateOrderDetail(orderDetail) {
        $('#input-update-quantity').val(orderDetail.quantityOrder);
        $('#input-update-price').val(orderDetail.priceEach);

        $('#btn-update-orderDetail').off('click').on('click', () => {
            this._clearInValid();
            let isValid = true;
            const requiredFields = ['input-update-quantity', 'input-update-price'];

            requiredFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass('is-invalid');
                    $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
                }
            });

            if (isValid) {
                const updatedOrderDetail = {
                    quantityOrder: $('#input-update-quantity').val().trim(),
                    priceEach: parseFloat($("#input-update-price").val().trim()),
                };

                this.vApi.onUpdateOrderDetailClick(orderDetail.id, updatedOrderDetail, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }


    _deleteOrderDetail(data) {
        $("#btn-confirm-delete-orderDetail").on("click", () => {
            this.vApi.onDeleteOrderDetailClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-orderDetail-form").find("input").val("");
        $("#create-orderDetail-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createOrderDetail()
        } else {
            if (type === "edit") {
                this._updateOrderDetail(data)
                $("#update-orderDetail-modal").modal("show")
            } else {
                this._deleteOrderDetail(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
    onGetPhotosClick(paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetPhotoByIdClick(photoId, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetOrderDetailsClick(paramCallbackFn) {
        $.ajax({
            url: gORDERDETAIL_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetOrderDetailByIdClick(orderDetailId, paramCallbackFn) {
        $.ajax({
            url: gORDERDETAIL_URL + "/" + orderDetailId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateOrderDetailClick(orderDetailData, paramCallbackFn) {
        $.ajax({
            url: gORDERDETAIL_URL,
            method: 'POST',
            data: JSON.stringify(orderDetailData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo orderDetail thành công!!")
                $("#create-orderDetail-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateOrderDetailClick(orderDetailId, orderDetailData, paramCallbackFn) {
        $.ajax({
            url: gORDERDETAIL_URL + "/" + orderDetailId,
            method: 'PUT',
            data: JSON.stringify(orderDetailData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-orderDetail-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa orderDetail thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteOrderDetailClick(orderDetailId) {
        $.ajax({
            url: gORDERDETAIL_URL + "/" + orderDetailId,
            method: 'DELETE',
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa orderDetail thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/order-details/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}