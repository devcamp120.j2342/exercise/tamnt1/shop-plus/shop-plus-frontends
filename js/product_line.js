/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPRODUCTLINE_URL = "http://localhost:8080/api/product-lines";

const gCOLUMN_ID = {
    stt: 0,
    action:1,
    productLine: 2,
    description: 3,


}
const gCOL_NAME = [
    "stt",
    "action",
    "productLine",
    "description",

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getproductLineList() {
        this.vApi.onGetProductLinesClick((paramproductLine) => {
            console.log(paramproductLine)
            this._createProductLineTable(paramproductLine)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createProductLineTable(paramproductLine) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-productLine')) {
            $('#table-productLine').DataTable().destroy();
        }
        const vOrderTable = $("#table-productLine").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.productLine] },
                {
                    data: gCOL_NAME[gCOLUMN_ID.description],
                    render: function (data, type, row) {
                      if (type === "display" && data.length > 50) {
                        return data.substr(0, 50) + "...";
                      }
                      return data;
                    },
                  },

            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-productLine" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-productLine" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-productLine').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-productLine').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramproductLine) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "product_line.xlsx");
            });
        });
    }

    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getproductLineList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-productLine").on("click", () => {
            $("#create-productLine-modal").modal("show")
  
        })
    }
    _createProductLine() {
        this._clearInput();
        $("#btn-create-productLine").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
      
          const vFields = [
            "input-create-productLineCode",
            "input-create-desc",
          ];
      
          vFields.forEach((field) => {
            const fieldValue = $(`#${field}`).val().trim();
      
            if (!fieldValue || fieldValue === "") {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const productLineData = {
              productLine: $("#input-create-productLineCode").val().trim(),
              description: $("#input-create-desc").val().trim(),
            };
      
            this.vApi.onCreateProductLineClick( productLineData, (responseData) => {
              console.log(responseData);
            });
          }
        }).bind(this);
      }
      
      _updateProductLine(data) {
        $("#input-update-productLineCode").val(data.productLine);
        $("#input-update-desc").val(data.description);
      
        $('#btn-update-productLine').off('click').on('click', () => {
          this._clearInValid();
      
          let isValid = true;
      
          const requiredFields = [
            'input-update-productLineCode',
            'input-update-desc',
          ];
      
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass('is-invalid');
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
      
          if (isValid) {
            const updatedProductLine = {
              productLine: $("#input-update-productLineCode").val().trim(),
              description: $("#input-update-desc").val().trim(),
            };
      
            this.vApi.onUpdateProductLineClick(data.id, updatedProductLine, (responseData) => {
              console.log(responseData);
            });
          }
        }).bind(this);
      }
      
    _deleteProductLine(data) {
        $("#btn-confirm-delete-productLine").on("click", () => {
            this.vApi.onDeleteproductLineClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-productLine-form").find("input").val("");
        $("#create-productLine-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createProductLine()
        } else {
            if (type === "edit") {
                this._updateProductLine(data)
                $("#update-productLine-modal").modal("show")
            } else {
                this._deleteProductLine(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
    onGetPhotosClick(paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetPhotoByIdClick(photoId, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetProductLinesClick(paramCallbackFn) {
        $.ajax({
            url: gPRODUCTLINE_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetproductLineByIdClick(productLineId, paramCallbackFn) {
        $.ajax({
            url: gPRODUCTLINE_URL + "/" + productLineId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateProductLineClick(productLineData, paramCallbackFn) {
        $.ajax({
            url: gPRODUCTLINE_URL,
            method: 'POST',
            data: JSON.stringify(productLineData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo productLine thành công!!")
                $("#create-productLine-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateProductLineClick(productLineId, productLineData, paramCallbackFn) {
        $.ajax({
            url: gPRODUCTLINE_URL + "/" + productLineId,
            method: 'PUT',
            data: JSON.stringify(productLineData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-productLine-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa productLine thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteproductLineClick(productLineId) {
        $.ajax({
            url: gPRODUCTLINE_URL + "/" + productLineId,
            method: 'DELETE',
            success:  ()=> {
                this.onShowToast("Xóa thành công", "bạn đã xóa productLine thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/product-lines/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}