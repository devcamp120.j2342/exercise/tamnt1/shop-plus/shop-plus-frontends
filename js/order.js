/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gORDER_URL = "http://localhost:8080/api/order";
const gCUSTOMER_URL = "http://localhost:8080/api/customers";

const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    comments: 2,
    orderDate: 3,
    requiredDate: 4,
    shippedDate: 5,
    status: 6,
    customer:7,

}
const gCOL_NAME = [
    "action",
    "id",
    "comments",
    "orderDate",
    "requiredDate",
    "shippedDate",
    "status",
    "customer"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4',
                tags: true,
				dropdownParent: $("#create-order-modal")
              })
       
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getOrderList() {
        this.vApi.onGetOrdersClick((paramOrder) => {
            this._createOrderTable(paramOrder)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createOrderTable(paramOrder) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-order')) {
            $('#table-order').DataTable().destroy();
        }
        const vOrderTable = $("#table-order").DataTable({
            // Khai báo các cột của datatable
            processing: true, // shows loading image while fetching data
            serverSide: true, // activates server side pagination
            ajax: {
                // headers: {
                //     Authorization: "Bearer " + this.token,
                // },
                url: gORDER_URL + "/dataTable", // API
            },
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            columns: [{ data: gCOL_NAME[0] }, { data: gCOL_NAME[1] }, { data: gCOL_NAME[2],    render: function (data, type, row) {
                // Modify this column to show the name from the "customer" object
                console.log(row.comments)
                if (row.comments === "null") {
                   
                    return '';
                }
               return row.comments
            }}, { data: gCOL_NAME[3] }, { data: gCOL_NAME[4] }, { data: gCOL_NAME[5] }, { data: gCOL_NAME[6] }, {data: gCOL_NAME[7], 
                render: function (data, type, row) {
                    // Modify this column to show the name from the "customer" object
                    if (row.customer) {
                        return row.customer.firstName + ' ' + row.customer.lastName;
                    }
                    return '';
                }}],
            order: [[1, "asc"]],
            scrollX: true,
     
            // Ghi đè nội dung của cột action
            "columnDefs": [ {

                    targets: 0,
                    defaultContent: `
                              <img class="edit-order" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-order" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === 0) {
                            $(cell).find('.edit-order').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-order').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        $('.table-container').css('overflow-x', 'auto');
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "order.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getOrderList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-order").on("click", () => {
            $("#create-order-modal").modal("show")
        })
    }
    _renderCustomerList() {
        const selectElement = $("#select-customer");
        selectElement.empty();

        this.vApi.onGetCustomersClick(1,60,(customerList) => {
            customerList.forEach((customer) => {
                    selectElement.append(`<option value="${customer.id}">${customer.firstName} ${customer.lastName}</option>`);

            });
        });
    }
    _createOrder() {
        this._clearInput();
        $("#btn-create-order").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
          const selectedCustomerId = parseInt($("#select-customer").val());
          const vFields = [
            "input-create-comment",
            "input-create-requiredDate",
            "input-create-shippedDate",
            "input-create-status",
          ];
      

            vFields.forEach((field) => {
              const fieldValue = $(`#${field}`).val();
      
              if (!fieldValue || fieldValue.trim() === "") {
                isValid = false;
                $(`#${field}`).addClass("is-invalid");
                $(`#${field}`).after(
                  `<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`
                );
              }
            });
            if (isValid) {
                this.vApi.onGetCustomerByIdClick(selectedCustomerId, (customer) => {
                    const selectedCustomer = {
                      id: customer.id,
                      lastName: customer.lastName,
                      firstName: customer.firstName,
                      phoneNumber: customer.phoneNumber,
                      city: customer.city,
                      state: customer.state,
                      postalCode: customer.postalCode,
                      country: customer.country,
                      salesRepEmployeeNumber: customer.salesRepEmployeeNumber || 0,
                      creditLimit: customer.creditLimit || 0,
                    };

                    const orderData = {
                        comments: $("#input-create-comment").val().trim(),
                        requiredDate: $("#input-create-requiredDate").val().trim(),
                        shippedDate: $("#input-create-shippedDate").val().trim(),
                        status: $("#input-create-status").val(),
                        customer: selectedCustomer,
                      };
                      this.vApi.onCreateOrderClick(orderData, (data) => {
                        console.log(data);
                      });
                });
         
            }
        });
      }
      _updateOrder(data) {
        const requiredDate = new Date(data.requiredDate).toISOString().split("T")[0];
        const shippedDate = new Date(data.shippedDate).toISOString().split("T")[0];
        
        $("#input-update-comment").val(data.comments);
        $("#input-update-requiredDate").val(requiredDate);
        $("#input-update-shippedDate").val(shippedDate);
        $("#input-update-status").val(data.status);
        $('#btn-update-order').off('click').on('click', () => {
          this._clearInValid();
        
          let isValid = true;
          const requiredFields = [
            'input-update-comment',
            'input-update-requiredDate',
            'input-update-shippedDate',
            'input-update-status',
          ];
        
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass('is-invalid');
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
        
          if (isValid) {
            const updatedOrder = {
              comments: $("#input-update-comment").val().trim(),
              requiredDate: $("#input-update-requiredDate").val().trim(),
              shippedDate: $("#input-update-shippedDate").val().trim(),
              status: $("#input-update-status").val(),
              orderDetail:[],
              customer: data.customer
            };
            this.vApi.onUpdateOrderClick(data.id, updatedOrder, (responseData) => {
              console.log(responseData);
            });
          }
        }).bind(this);
      }
      

    _deleteOrder(data) {
        $("#btn-confirm-delete-order").on("click", () => {
            this.vApi.onDeleteOrderClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-order-form").find("input").val("");
        $("#create-order-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._renderCustomerList()
            this._createOrder()
        } else {
            if (type === "edit") {
                this._updateOrder(data)
                $("#update-order-modal").modal("show")
            } else {
                this._deleteorder(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetCustomerByIdClick(paramCustomerId, paramCallbackFn) {
        $.ajax({
            url:  gCUSTOMER_URL + "/" + paramCustomerId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetCustomersClick(paramPage, paramSize, paramCallbackFn) {
        $.ajax({
            url: `${gCUSTOMER_URL}?page=${paramPage}&size=${paramSize}`,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }


    onGetPhotoByIdClick(photoId, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetOrdersClick(paramCallbackFn) {
        $.ajax({
            url: gORDER_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetOrderByIdClick(orderId, paramCallbackFn) {
        $.ajax({
            url: gORDER_URL + "/" + orderId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateOrderClick(orderData, paramCallbackFn) {
        $.ajax({
            url: gORDER_URL,
            method: 'POST',
            data: JSON.stringify(orderData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo order thành công!!")
                $("#create-order-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateOrderClick(orderId, orderData, paramCallbackFn) {
        $.ajax({
            url: gORDER_URL + "/" + orderId,
            method: 'PUT',
            data: JSON.stringify(orderData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-order-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa order thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteOrderClick(orderId) {
        $.ajax({
            url: gORDER_URL + "/" + orderId,
            method: 'DELETE',
            success: function () {
                this.onShowToast("Xóa thành công", "bạn đã xóa order thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/orders/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}