/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gCUSTOMER_URL = "http://localhost:8080/api/customers";
const gCOLUMN_ID = {
    action:0,
    stt: 1,
    lastName: 2,
    firstName: 3,
    phoneNumber: 4,
    address: 5,
    city: 6,
    state: 7,
    postalCode:8,
    country:9,
    salesRepEmployeeNumber: 10,
    creditLimit:11,

}
const gCOL_NAME = [
    "action",
    "id",
    "lastName",
    "firstName",
    "phoneNumber",
    "address",
    "city",
    "state",
    "postalCode",
    "country",
    "salesRepEmployeeNumber",
    "creditLimit"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }



}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getCustomerList() {
        this.vApi.onGetCustomersClick((paramcustomer) => {
            this._createCustomerTable(paramcustomer)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createCustomerTable(paramcustomer) {
        let stt = 1;
        if ($.fn.DataTable.isDataTable('#table-customer')) {
            $('#table-customer').DataTable().destroy();
        }
        const vOrderTable = $("#table-customer").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            processing: true, // shows loading image while fetching data
            serverSide: true, // activates server side pagination
            ajax: {
                // headers: {
                //     Authorization: "Bearer " + this.token,
                // },
                url: gCUSTOMER_URL + "/dataTable", // API
            },
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            columns: [{ data: gCOL_NAME[0] }, { data: gCOL_NAME[1] }, { data: gCOL_NAME[2] }, { data: gCOL_NAME[3] }, { data: gCOL_NAME[4] }, { data: gCOL_NAME[5] }, { data: gCOL_NAME[6] }, { data: gCOL_NAME[7] }, { data: gCOL_NAME[8] }, { data: gCOL_NAME[9] }, { data: gCOL_NAME[10] }, { data: gCOL_NAME[11] }],
            order: [[1, "asc"]],
            scrollX: true,
            // Ghi đè nội dung của cột action
            "columnDefs": [
    
                {
                    targets: 0,
                    defaultContent: `
                        <img class="edit-customer" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                        <img class="delete-customer" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                    `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-customer').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });
    
                            $(cell).find('.delete-customer').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData);
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }
            ]
        });
        $('.table-container').css('overflow-x', 'auto');
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)'); // Add buttons to the desired container
    }
    
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "users.xlsx");
            });
        });
    }
    _getCustomerPerCountry() {
      
    }

    _createBarChart() {
        this.vApi.onGetCustomersPerCountry((customer_data) => {
        
          customer_data.sort((a, b) => b[1] - a[1]);
      
          const top10Countries = customer_data.slice(0, 10);
      
          var bar_data = {
            data: top10Countries.map(([_, count], index) => [index + 1, count]),
            bars: { show: true }
          };

          $.plot('#bar-chart', [bar_data], {
            grid: {
              borderWidth: 1,
              borderColor: '#f3f3f3',
              tickColor: '#f3f3f3'
            },
            series: {
              bars: {
                show: true,
                barWidth: 0.5,
                align: 'center'
              }
            },
            colors: ['#3c8dbc'],
            xaxis: {
              ticks: top10Countries.map(([country], index) => [index + 1, country])
            }
          });
        });
      }
      
      
    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getCustomerList()
        this._createBarChart()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-customer").on("click", () => {
            $("#create-customer-modal").modal("show")
        })
    }


    _createCustomer() {
        this._clearInput();
        $("#btn-create-customer").on("click", () => {
            this._clearInValid();
    
            let isValid = true;
            const vFields = [
                "input-create-customer-firstName",
                "input-create-customer-lastName",
                "input-create-customer-phone",
                "input-create-customer-address",
                "input-create-customer-city",
                "input-create-customer-state",
                "input-create-customer-postal",
                "input-create-customer-country"
            ];
    
            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });
    
            if (isValid) {
                const customerData = {
                    lastName: $("#input-create-customer-lastName").val().trim(),
                    firstName: $("#input-create-customer-firstName").val().trim(),
                    phoneNumber: $("#input-create-customer-phone").val().trim(),
                    address: $("#input-create-customer-address").val().trim(),
                    city: $("#input-create-customer-city").val().trim(),
                    state: $("#input-create-customer-state").val().trim(),
                    postalCode: $("#input-create-customer-postal").val().trim(),
                    country: $("#input-create-customer-country").val().trim()
                };
                this.vApi.onCreateCustomerClick(customerData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }
    
    _updateCustomer(customer) {
        $('#input-update-customer-lastName').val(customer.lastName);
        $('#input-update-customer-firstName').val(customer.firstName);
        $('#input-update-customer-phone').val(customer.phoneNumber);
        $('#input-update-customer-address').val(customer.address);
        $('#input-update-customer-city').val(customer.city);
        $('#input-update-customer-state').val(customer.state);
        $('#input-update-customer-postal').val(customer.postalCode);
        $('#input-update-customer-country').val(customer.country);
        $('#btn-update-customer').off('click').on('click', () => {
            this._clearInValid();
            let isValid = true;
            const requiredFields = ['input-update-customer-firstName', 'input-update-customer-lastName', 'input-update-customer-phone', 'input-update-customer-address', 'input-update-customer-city', 'input-update-customer-state', 'input-update-customer-postal', 'input-update-customer-country'];
    
            requiredFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass('is-invalid');
                    $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
                }
            });
    
            if (isValid) {
                const updatedCustomer = {
                    lastName: $('#input-update-customer-lastName').val().trim(),
                    firstName: $('#input-update-customer-firstName').val().trim(),
                    phoneNumber: $('#input-update-customer-phone').val().trim(),
                    address: $('#input-update-customer-address').val().trim(),
                    city: $('#input-update-customer-city').val().trim(),
                    state: $('#input-update-customer-state').val().trim(),
                    postalCode: $('#input-update-customer-postal').val().trim(),
                    country: $('#input-update-customer-country').val().trim()
                };
    
               
               this.vApi.onUpdateCustomerClick(customer.id, updatedCustomer,(data)=> {
                console.log(data)
               })
            }
        }).bind(this);
    }
    

    _deleteCustomer(data) {
        $("#btn-confirm-delete-customer").on("click", () => {
          this.vApi.onDeleteCustomerClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-customer-form").find("input").val("");
        $("#create-customer-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createCustomer()
        } else {
            if (type === "edit") {
                this._updateCustomer(data)
                $("#update-customer-modal").modal("show")
            } else {
                this._deleteCustomer(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetCustomersClick(paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetCustomersPerCountry(paramCallbackFn) {
        $.ajax({
            url: `${gCUSTOMER_URL}/country`,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/customers/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetCustomerByIdClick(customerId, paramCallbackFn) {
        $.ajax({
            url: gcustomer_URL + "/" + customerId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateCustomerClick(customerData, paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL,
            method: 'POST',
            data: JSON.stringify(customerData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo customer thành công!!")
                $("#create-customer-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateCustomerClick(customerId, customerData, paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL + "/" + customerId,
            method: 'PUT',
            data: JSON.stringify(customerData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-customer-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa customer thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteCustomerClick(customerId) {
        $.ajax({
            url: gCUSTOMER_URL + "/" + customerId,
            method: 'DELETE',
            success:  () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa customer thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    
}