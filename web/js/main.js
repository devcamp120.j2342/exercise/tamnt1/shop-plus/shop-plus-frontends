/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:5500/shop-plus-frontends/web"
const gPRODUCT_URL = "http://localhost:8080/api/products";
const gPRODUCT_LINE_URL = "http://localhost:8080/api/product-lines";
// constant pagination
const PAGE = 0
const SIZE = 6

$(document).ready(function () {
  "use strict";
  const quantityInput = $('.qty-text');
  const qtyMinusBtn = $('.qty-minus');
  const qtyPlusBtn = $('.qty-plus');
  // Add event listener using .on("click") for quantity minus button
  qtyMinusBtn.on("click", function () {
    if (parseInt(quantityInput.val()) > 1) {
      quantityInput.val(parseInt(quantityInput.val()) - 1);
    }
  });

  // Add event listener using .on("click") for quantity plus button
  qtyPlusBtn.on("click", function () {
    if (parseInt(quantityInput.val()) < 300) {
      quantityInput.val(parseInt(quantityInput.val()) + 1);
    }
  });

  $("#addtocart").on("click", function (event) {
    event.preventDefault();

    console.log(quantityInput.val())

  });
  let searchParams = new URLSearchParams(window.location.search);
  // Get query parameters
  const productVendor = getQueryParam("productVendor");
  const productLineId = getQueryParam("productLineId");
  const priceFrom = getQueryParam("priceFrom");
  const priceTo = getQueryParam("priceTo");
  const productId = getQueryParam("id")

  // Construct filter data object
  const data = {
    productVendor,
    productLineId: parseInt(productLineId),
    priceFrom,
    priceTo
  };

  // Initialize filter data
  let filterData = {
    productVendor: "",
    productLineId: null,
    priceFrom: null,
    priceTo: null,
    size: 6
  };


  // Initial load of products
  onGetProductByFilter(PAGE, data, (paramProducts) => {
    renderProducts(PAGE, filterData.size, paramProducts);
  });

  // Get product lines and render categories
  onGetProductLineClick((paramProductLines) => {
    renderProductCategories(paramProductLines);
  });

  // Handle size change
  $("#viewProduct").on("change", function (event) {
    event.preventDefault();
    const value = event.target.value;
    searchParams.set("size", value);
    filterData = {
      ...filterData,
      size: value
    };
    updateFilterParams();
    fetchAndRenderProducts(PAGE, value);
  });

  // Handle category click
  $(".categories-container").on("click", ".category-item", function (event) {
    event.preventDefault();
    $(".category-item").removeClass("active");
    $(this).addClass("active");
    const productLineId = $(this).data("category-id");
    searchParams.set("productLineId", productLineId);
    filterData = {
      ...filterData,
      productLineId: productLineId
    };
    updateFilterParams();

    fetchAndRenderProducts(PAGE, filterData.size);
  });

  // Handle checkbox filter change
  $(".widget-desc").on("change", ".form-check-input", function () {
    if (this.checked) {
      $(".form-check-input").not(this).prop("checked", false);
      const checkboxValue = $(this).val();
      searchParams.set("productVendor", checkboxValue);
      updateFilterParams();
      filterData = {
        ...filterData,
        productVendor: checkboxValue
      };
      console.log(filterData)
      fetchAndRenderProducts(PAGE, filterData.size);
    }
  });

  // Handle price range filter change
  $(".slider-range-price").on("slidechange", function (event, ui) {
    const minPrice = ui.values[0];
    const maxPrice = ui.values[1];
    filterData = {
      ...filterData,
      priceFrom: minPrice,
      priceTo: maxPrice
    };
    searchParams.set("priceFrom", minPrice);
    searchParams.set("priceTo", maxPrice);
    updateFilterParams();
    if (minPrice && maxPrice) {
      fetchAndRenderProducts(PAGE, filterData.size);
    }
  });

  // Handle reset button click
  $(".btn-reset").on("click", function (event) {
    event.preventDefault();
    resetFilterData();
    resetUrl();
    resetUIElements();
    fetchAndRenderProducts(PAGE, sizeFrom);
  });


  $(".pagination-container").on("click", ".page-link", function (event) {
    event.preventDefault();
    const clickedPage = parseInt($(this).attr("data-page"));
    fetchAndRenderProducts(clickedPage, filterData.size);
  });

  function fetchAndRenderProducts(page, size) {
    onGetProductByFilter(page, filterData, (paramProducts) => {
      renderProducts(page, size, paramProducts);
    });
  }
  function updateFilterParams() {
    const propertyFilterUrl = `${gBASE_URL}/shop.html?${searchParams.toString()}`;
    history.pushState(null, "", propertyFilterUrl);
  }

  function resetFilterData() {
    filterData = {
      productVendor: "",
      productLineId: null,
      priceFrom: null,
      priceTo: null
    };
  }

  if (productId) {
    onGetProductDetailClick(productId, (productDetail) => {
      renderProductDetail(productDetail)
    })
  }




});

//render list of product
function renderProducts(currentPage, size, paramProducts) {
  const totalPages = Math.ceil(paramProducts.totalCount / size);
  const productGrid = $(".product-grid");
  productGrid.empty()
  paramProducts.data.forEach(product => {
    const productHtml = `
      <div class="col-12 col-sm-6 col-md-12 col-xl-6">
        <div class="single-product-wrapper">
         <a href="${gBASE_URL}/product-details.html?id=${product.id}" class="product-img" style="height: 400px; display: block; cursor: pointer;" data-product-id=${product.id}>
          <img src="${product.productPhotos[0] || getRandomProductPhoto()}" alt="" style="object-fit: cover; height: 100%; width: 100%;">
            <img class="hover-img" src="${product.productPhotos[1] || getRandomProductPhoto()}" alt="">
          </a>
          <div class="product-description d-flex align-items-center justify-content-between">
            <div class="product-meta-data">
              <div class="line"></div>
              <p class="product-price">$${product.buyPrice}</p>
              <a href="product-details.html">
                <h6>${product.productName}</h6>
              </a>
            </div>
            <div class="ratings-cart text-right">
              <div class="ratings">
              <i class="fa fa-star" aria-hidden="true"></i>
              <i class="fa fa-star" aria-hidden="true"></i>
              <i class="fa fa-star" aria-hidden="true"></i>
              <i class="fa fa-star" aria-hidden="true"></i>
              <i class="fa fa-star" aria-hidden="true"></i>
              </div>
              <div class="cart">
                <a href="cart.html" data-toggle="tooltip" data-placement="left" title="Add to Cart">
                  <img src="img/core-img/cart.png" alt="">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>`;
    productGrid.append(productHtml);
  });
  const paginationHtml = createPagination(totalPages, currentPage);
  $(".pagination-container").html(paginationHtml);
}
//render categories
function renderProductCategories(categories) {
  const categoriesContainer = $(".categories-container");
  categoriesContainer.empty();
  categories.forEach(category => {
    const categoryHtml = `
      <li class=""><a href="#" class="category-item" data-category-id="${category.id}">${category.productLine}</a></li>
    `;
    categoriesContainer.append(categoryHtml);
  });
}
//render product detail
function renderProductDetail(product) {
  const arrayPhoto = product.productPhotos
  $(".product-title").text(product.productName)
  $(".product-name").text(product.productName)
  $(".product-price").text(product.buyPrice)
  $(".avaibility").text(product.quantityInStock > 0 ? "In stock" : " Out Of Stock")
  $(".short_overview").text(product.productDescription)


  // Select the carousel indicators and inner container
  const carouselIndicators = $(".carousel-indicators");
  const carouselInner = $(".carousel-inner");
  // Clear existing carousel indicators and inner content
  carouselIndicators.empty();
  carouselInner.empty();

  // Loop through the array of product photos
  product.productPhotos.forEach((image, index) => {
    // Create carousel indicator
    const indicator = $(`<li data-target="#product_details_slider" data-slide-to="${index}" style="background-image: url(${image});"></li>`);
    carouselIndicators.append(indicator);

    // Create carousel item
    const itemClass = index === 0 ? "carousel-item active" : "carousel-item";
    const item = $(`
        <div class="${itemClass}">
          <a class="gallery_img" href="${image}">
            <img class="d-block w-100" src="${image}" alt="Slide ${index + 1}">
          </a>
        </div>
      `);
    carouselInner.append(item);
  });

}


function createPagination(totalPages, currentPage) {
  const paginationContainer = $(".pagination-container");
  paginationContainer.empty(); // Clear the existing pagination

  let paginationHtml = `
    <ul class="pagination justify-content-end mt-50 pagination-container">
  `;

  for (let page = 0; page < totalPages; page++) { // Start from 0
    const pageNumber = page + 1; // Adjust the page number for display
    paginationHtml += `
      <li class="page-item ${currentPage === page ? 'active' : ''}">
        <a class="page-link" href="#" data-page="${page}">${pageNumber}</a>
      </li>
    `;
  }

  paginationHtml += `
    </ul>
  `;

  paginationContainer.append(paginationHtml);
}
/*** REGION 4 -   API CALL - Vùng gọi api trả về data */
function onGetProductClick(page, size, paramCallbackFn) {
  $.ajax({
    url: `${gPRODUCT_URL}?page=${page}&size=${size}`,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}

function onGetProductDetailClick(productId, paramCallbackFn) {
  $.ajax({
    url: `${gPRODUCT_URL}/${productId}`,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}
function onGetProductByProductLineClick(page, size, productLineId, paramCallbackFn) {
  $.ajax({
    url: `${gPRODUCT_URL}/${productLineId}/productLine?page=${page}&size=${size}`,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}
function onGetProductByFilter(page, data, paramCallbackFn) {
  const { productLineId, productVendor, priceFrom, priceTo, size } = data;
  // Construct the URL with the provided filter data
  const url = `${gPRODUCT_URL}/filter?productLineId=${productLineId || ""}&productVendor=${productVendor || ""}&priceFrom=${priceFrom || ''}&priceTo=${priceTo || ''}&page=${page}&size=${size ? size : 6}`;
  $.ajax({
    url: url,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
      $("body").addClass("box-collapse-closed")
      $("body").removeClass("box-collapse-open")
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}


function onGetProductLineClick(paramCallbackFn) {
  $.ajax({
    url: `${gPRODUCT_LINE_URL}`,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}

/*** REGION 5 - UTILITY - Vùng  UTILITY */



function getQueryParam(name) {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  return urlParams.get(name);
}

function getRandomProductPhoto() {
  const randomIndex = Math.floor(Math.random() * products.length);
  return products[randomIndex];
}

const products = [
  "https://media.istockphoto.com/id/1199024795/vector/no-image-vector-symbol-missing-available-icon-no-gallery-for-this-moment.jpg?s=170667a&w=0&k=20&c=OOGRbw9GNagCFYI4egFt26chL8F9V59tqfSrKx0jyJQ="

]